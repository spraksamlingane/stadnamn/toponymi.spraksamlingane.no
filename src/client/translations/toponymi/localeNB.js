export default {
  languageLabel: 'Bokmål',
  appTitle: {
    short: 'Norske stedsnavn',
    long: 'Norske stedsnavn'
  },
  appDescription1: `
  Søk, analyser og visualiser stedsnavndata.
  `,
  appDescription2: `
  For å starte et søk, velg ønsket stedsnavntema fra menyen og skriv inn et stednavn. Alternativt kan du hente alle stedsnavn fra kartområdet.
  `,
  appDescription3: `
  Søk kan også kombineres med ordet 'eller'. For eksempel kan "*tjønn eller tjern*" finne alle navn som slutter på '-tjønn' eller '-tjern'.
  `,
  appDescription4: `
  `,
  topBar: {
    feedback: 'Tilbakemelding',
    accessibility: "Tilgjengelighetserklæring",
    info: {
      info: 'Om',
      blog: '',
      blogUrl: ''
    }
  },
  facetBar: {
    results: 'Treff',
    narrowDownBy: 'Filtrer treff etter',
    filters: 'filtrer treff'

  },
  tabs: {
    table: 'Tabell',
    map_clusters: 'Klyngekart',
    map_markers: 'Punktkart',
    heatmap: 'Konsentrasjonskart',
    statistics: 'Statistikk',
    download: 'last ned'
  },
  table: {
    rowsPerPage: 'Rader per side',
    of: 'av'
  },
  resultsAsCSV: 'resultat som csv fil',
  facets: {
    dateFacet: {
      invalidDate: 'Ugyldig dato.',
      toBeforeFrom: 'Startdato må være før sluttdato.',
      minDate: 'Den tidligste tillate dato er {minDate}',
      maxDate: 'Den siste tillatte datoen er {maxDate}',
      cancel: 'Avbryt',
      fromLabel: 'Fra',
      toLabel: 'Til'
    },
    textFacet: {
      inputLabel: 'Søk etter navns'
    }
  },
  leafletMap: {
    basemaps: {
      openStreetMap: 'Verdenskart (Open Street Map)',
      topographicalMapKV: 'Norgeskart, farger (Kartverket)',
      greytoneMapKV: 'Norgeskart, gråtone (Kartverket)',
      terrainMapKV: 'Terrengkart (Kartverket)',
      stamenTonerMap: 'Verdenskart, svart-hvitt (Stamen)',
      stamenWatercolorMap: 'Verdenskart, vannfarge (Stamen)'
    },
    externalLayers: {
      // arkeologiset_kohteet_alue: 'Register of Archaeological Sites, areas',
      // arkeologiset_kohteet_piste: 'Register of Archaeological Sites, points',
      karelianMaps: 'Karjalan kartat, 1:100 000 topografinen (SeCo)',
      senateAtlas: 'Senaatin kartasto, 1:21 000 topografinen (SeCo)',
      'kotus:pitajat': 'Pitäjät: Suomi, Viro, ja raja-alueet, 1:1 000 000, 1938 (Kotus)',
      'kotus:rajat-sms-alueet': 'Murrealueet (Kotus)',
      'kotus:rajat-sms-alueosat': 'Murrealueiden osat (Kotus)',
      'kotus:rajat-lansi-ita': 'Länsi- ja itämurteiden raja (Kotus)'
    },
    mapModeButtons: {
      markers: 'Markers',
      heatmap: 'Heatmap'
    },
    wrongZoomLevel: 'Kartets zoomnivå må være minst 11.',
    tooManyResults: 'Over 500 søkeresultat, bruk klyngekart.'
  },
  instancePageGeneral: {
    introduction: `
      <p class="MuiTypography-root MuiTypography-body1 MuiTypography-paragraph">
        This landing page provides a human-readable summary of the data points that link
        to this {entity}. The data included in this summary reflect only those data points
        used in the MMM Portal. Click the Open in Linked Data Browser on button on the
        Export tab to view the complete set of classes and properties linked to this record.
      </p>
      <p class="MuiTypography-root MuiTypography-body1 MuiTypography-paragraph">
        To cite this record, use its url. You can use also use the url to return directly
        to the record at any time.
      </p>
    `,
    repetition: `
      <h6 class="MuiTypography-root MuiTypography-h6">
        Repetition of data
      </h6>
      <p class="MuiTypography-root MuiTypography-body1 MuiTypography-paragraph">
        The same or similar data may appear within a single data field multiple times.
        This repetition occurs due to the merging of multiple records from different datasets
        to create the MMM record.
      </p>
    `
  },
  perspectives: {
    clientFSPlaces: {
      datasets: {
        bsn: {
          label: 'Bustadnamnregisteret (BSN)',
          aboutLink: 'https://storymaps.arcgis.com/stories/563e56e4d3604a299626c8e3993d2332'
        },
        bustadnamnregisteret: {
          label: 'Bustadnamnregisteret GAMMEL (BSN)',
          aboutLink: 'https://storymaps.arcgis.com/stories/563e56e4d3604a299626c8e3993d2332'
        },
        sognogfjordane: {
          label: 'Stadnamn Sogn og Fjordane (SOF)',
          aboutLink: 'https://www.fylkesarkivet.no/stadnamn.380535.no.html'
        },
        tgn: {
          label: 'The Getty Thesaurus of Geographic Names (TGN)',
          aboutLink: 'http://www.getty.edu/research/tools/vocabularies/tgn/about.html'
        },
        m1886: {
          label: "Matrikkelen av 1886 (M1886)",
          aboutLink :"https://uib.no"
        },
        mu1950: {
          label: "Matrikkelutkastet av 1950 (MU1950)",
          aboutLink :"https://uib.no"
        },
        rygh: {
          label: "Oluf Rygh: Norske Gaardnavne (RYGH)",
          aboutLink :"https://nn.wikipedia.org/wiki/Norske_Gaardnavne"
        }
      },
      facetResultsType: '',
      inputPlaceHolder: 'Søk etter stedsnavn',
      searchByArea: 'Søk etter alle stedsnavn i området',
      searchByAreaTitle: `
      Avgrens kartvisningen til søkeområdet. Sett zoomnivået til minst 11 og bruk søkeknappen nederst.
      `,
      searchByAreaCancel: 'Avbryt',
      searchByAreaSearch: 'Søk',
      properties: {
        datasetSelector: {
          label: 'Velg kilde',
          description: 'Søket utføres på alt valgt kildemateriale samtidig'
        },
        columnSelector: {
          label: 'Velg kolonner',
          description: 'Description'
        },
        prefLabel: {
          label: 'Navn',
          description: 'Navn'
        },
        typeLabel: {
          label: 'Type',
          description: 'Type'
        },
        fylkeLabel: {
          label: 'Region',
          description: 'Region'
        },
        kommuneLabel: {
          label: 'Kommune',
          description: 'Kommune'
        },
        modifier: {
          label: 'Utmerkingsledd',
          description: 'Utmerkingsledd'
        },
        basicElement: {
          label: 'Hovedledd',
          description: 'Hovedledd'
        },
        collectionYear: {
          label: 'År',
          description: 'År'
        },
        source: {
          label: 'Kilde',
          description: 'Kilde'
        },
        datasetCode: {
          label: 'Datasett',
          description: 'Datasett'
        },
        sourceType: {
          label: 'Kildetype',
          description: 'Kildetype'
        },
        seeAlso: {
          label: 'Se også',
          description: 'Se også'
        },
        admID: {
          label: 'Adm. ID',
          description: 'BESKRIVELSE'
        },
        coordinateInfo: {
          label: 'Koordinatinformasjon',
          description: 'Koordinatinformasjon'
        },
        landingPage: {
          label: 'Lenkede data',
          description: 'Lenkede data'
        },
        manifest: {
          label: 'IIIF-bildevisning',
          description: 'IIIF-bildevisning'
        }
      }
    }
  }
}
