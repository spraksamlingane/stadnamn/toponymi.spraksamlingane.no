export default {
  languageLabel: 'English',
  appTitle: {
    short: 'NameSampo',
    long: 'NameSampo: A Workbench for Toponomastic Research'
  },
  appDescription1: `
    NameSampo is a web service for searching and studying place names. The search results
    can be viewed as a table, on maps, and as statistical distributions.
    Historical and current background maps are provided.
  `,
  appDescription2: `
    Start by choosing source dataset(s) and input a place name on the search field.
    Alternatively you can search by area.
  `,
  appDescription3: `

  `,
  appDescription4: `

  `,
  topBar: {
    feedback: 'feedback',
    accessibility: "Accessibility statement",
    info: {
      info: 'Info',
      blog: 'Project blog',
      blogUrl: 'https://seco.cs.aalto.fi',
      aboutThePortal: 'About the Portal'
    },
    searchBarPlaceHolder: 'Search all content',
    searchBarPlaceHolderShort: 'Search',
    instructions: 'instructions'
  },
  facetBar: {
    results: 'Results',
    narrowDownBy: 'Filter results by',
    filters: 'Filter results'
  },
  tabs: {
    table: 'table',
    map_clusters: 'clustered map',
    map_markers: 'map',
    heatmap: 'heatmap',
    statistics: 'statistics',
    download: 'download'
  },
  table: {
    rowsPerPage: 'Rows per page',
    of: 'of'
  },
  exportToYasgui: 'open the result table query in yasgui sparql editor',
  openInLinkedDataBrowser: 'open in linked data browser',
  resultsAsCSV: 'download the search results as a CSV table',
  facets: {
    dateFacet: {
      invalidDate: 'Epäkelpo päivämäärä.',
      toBeforeFrom: 'Alkupäivämäärän täytyy olla ennen loppupäivämäärää.',
      minDate: 'Aikaisin sallittu päivämäärä on {minDate}',
      maxDate: 'Myöhäisin sallittu päivämäärä on {maxDate}',
      cancel: 'Peruuta',
      fromLabel: 'Alku',
      toLabel: 'Loppu'
    },
    textFacet: {
      inputLabel: 'Etsi nimellä'
    }
  },
  leafletMap: {
    basemaps: {
      openStreetMap: 'World Map (Open Street Map)',
      topographicalMapKV: 'Terrain Map (Kartverket)',
      greytoneMapKV: 'Norway, grayscale (Kartverket)',
      terrainMapKV: 'Terrain map (Kartverket)',
      stamenTonerMap: 'World map, toner (Stamen)',
      stamenWatercolorMap: 'World map, watercolor (Stamen)'
    },
    externalLayers: {
      // arkeologiset_kohteet_alue: 'Register of Archaeological Sites, areas',
      // arkeologiset_kohteet_piste: 'Register of Archaeological Sites, points',
      karelianMaps: 'Karelian maps, 1:100 000 topographic (SeCo)',
      senateAtlas: 'Senate atlas, 1:21 000 topographic (SeCo)',
      'kotus:pitajat': 'Finnish parishes in 1938 (Institute for the Languages of Finland)',
      'kotus:rajat-sms-alueet': 'Dialectical regions in Finland (Institute for the Languages of Finland)',
      'kotus:rajat-sms-alueosat': 'Dialectical subregions in Finland (Institute for the Languages of Finland)',
      'kotus:rajat-lansi-ita': 'Border between western and eastern dialects in Finland (Institute for the Languages of Finland)'
    },
    mapModeButtons: {
      markers: 'Markers',
      heatmap: 'Heatmap'
    },
    wrongZoomLevel: 'The map zoom level has to at least 11',
    tooManyResults: 'More than 3000 results, please use clustered map or heatmap'
  },
  instancePageGeneral: {
    introduction: `
      <p class="MuiTypography-root MuiTypography-body1 MuiTypography-paragraph">
        This landing page provides a human-readable summary of the data points that link
        to this {entity}. The data included in this summary reflect only those data points
        used in the MMM Portal. Click the Open in Linked Data Browser on button on the
        Export tab to view the complete set of classes and properties linked to this record.
      </p>
      <p class="MuiTypography-root MuiTypography-body1 MuiTypography-paragraph">
        To cite this record, use its url. You can use also use the url to return directly
        to the record at any time.
      </p>
    `,
    repetition: `
      <h6 class="MuiTypography-root MuiTypography-h6">
        Repetition of data
      </h6>
      <p class="MuiTypography-root MuiTypography-body1 MuiTypography-paragraph">
        The same or similar data may appear within a single data field multiple times.
        This repetition occurs due to the merging of multiple records from different datasets
        to create the MMM record.
      </p>
    `
  },
  perspectives: {
    clientFSPlaces: {
      datasets: {
        /*
        BUSTADNAMNREGISTERET = Farm Name Register
Stadnamn Sogn og Fjordane = Vestland County Archive
Rygh: Norske Gaardnavne = Norwegian Farm Names
Geonames 2019 = Geonames 2019
OpenStreetMap 2019 = OpenStreetMap 2019
Statens Kartverk 2016 = Norwegian Mapping and Cadastre 2016
Statens Kartverk 2019 = Norwegian Mapping and Cadastre 2019
Statens Kartverk 2020 = Norwegian Mapping and Cadastre 2020
Matrikkel 1838 = 1838 Cadastre
Matrikkel 1886 = 1886 Cadastre
Matrikkelutkast 1950 = 1950 Cadastral Draft
Matrikkel 2010 (GAB) = 2010 Cadastre (GAB)
Folketeljing 1900 = 1900 Census
Folketeljing 1910 = 1910 Census

        */
        bsn: {
          label: 'Farm Name Register (BSN)',
          aboutLink: 'https://storymaps.arcgis.com/stories/563e56e4d3604a299626c8e3993d2332'
        },
        bustadnamnregisteret: {
          label: 'Farm Name Register OLD (BSN)',
          aboutLink: 'https://storymaps.arcgis.com/stories/563e56e4d3604a299626c8e3993d2332'
        },
        sognogfjordane: {
          label: 'Vestland County Archive (SOF)',
          aboutLink: 'https://www.fylkesarkivet.no/stadnamn.380535.no.html'
        },
        tgn: {
          label: 'The Getty Thesaurus of Geographic Names (TGN)',
          aboutLink: 'http://www.getty.edu/research/tools/vocabularies/tgn/about.html'
        },
        m1886: {
          label: "1886 Cadastre (M1886)",
          aboutLink :"https://uib.no"
        },
        mu1950: {
          label: "1950 Cadastral draft (MU1950)",
          aboutLink :"https://uib.no"
        },
        rygh: {
          label: "Norwegian Farm Names (RYGH)",
          aboutLink :"https://nn.wikipedia.org/wiki/Norske_Gaardnavne"
        }
      },
      facetResultsType: '',
      inputPlaceHolder: 'Search place names',
      searchByArea: 'Search by area',
      searchByAreaTitle: `
        Limit the area by zooming and moving the map view, and use the buttoms on bottom for confirmation.
        The zoom level must be at least 11.
      `,
      searchByAreaCancel: 'Cancel',
      searchByAreaSearch: 'Search',
      properties: {
        datasetSelector: {
          label: 'Choose dataset(s)',
          description: 'Description'
        },
        columnSelector: {
          label: 'Choose column(s)',
          description: 'Description'
        },
        prefLabel: {
          label: 'Name',
          description: 'Description'
        },
        typeLabel: {
          label: 'Type',
          description: 'Description'
        },
        fylkeLabel: {
          label: 'Region',
          description: 'Description'
        },
        kommuneLabel: {
          label: 'Municipality',
          description: 'Description'
        },
        modifier: {
          label: 'Modifier',
          description: 'Description'
        },
        basicElement: {
          label: 'Base',
          description: 'Description'
        },
        collectionYear: {
          label: 'Year',
          description: 'Description'
        },
        source: {
          label: 'Source',
          description: 'Description'
        },
        datasetCode: {
          label: 'Dataset',
          description: 'Description'
        },
        sourceType: {
          label: 'Source type',
          description: 'Description '
        },
        seeAlso: {
          label: 'See also',
          description: 'Description'
        },
        admID: {
          label: 'Adm. ID',
          description: 'Description'
        },
        coordinateInfo: {
          label: 'Coordinate information',
          description: 'Coordinate information'
        },
        landingPage: {
          label: 'Linked data',
          description: 'Linked data'
        },
        manifest: {
          label: 'IIIF-resource',
          description: 'IIIF-resource'
        }
      }
    }
  }
}
