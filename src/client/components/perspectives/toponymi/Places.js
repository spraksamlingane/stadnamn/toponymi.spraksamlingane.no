import React from 'react'
import PropTypes from 'prop-types'
import Immutable from 'immutable'
import intl from 'react-intl-universal'
import { Route, Redirect } from 'react-router-dom'
import PerspectiveTabs from '../../main_layout/PerspectiveTabs'
import LeafletMap from '../../facet_results/LeafletMap'
import ResultInfo from '../../facet_results/ResultInfo'
import GMap from '../../facet_results/GMap'
import VirtualizedTable from '../../facet_results/VirtualizedTable'
import Pie from '../../facet_results/Pie.js'
import CSVButton from '../../facet_results/CSVButton'

class Places extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      zoomLevel: 5,
      position: [65.21, 12.4917],
    };
  }

  handleZoom(level) {
    this.setState({
      zoomLevel: level
    });
  }

  handlePosition(position) {
    this.setState({
      position: [position.lat, position.lng]
    });
  }


  render() {
    {
      const { rootUrl, perspective, clientFSState, layoutConfig} = this.props
      const { maps } = clientFSState
      const { clientFSMapClusters, clientFSMapMarkers } = maps
      return (
        <>
          <PerspectiveTabs
            routeProps={this.props.routeProps}
            tabs={perspective.tabs}
            screenSize={this.props.screenSize}
            layoutConfig={layoutConfig}
          />
          <Route
            exact path={`${rootUrl}/app`}
            render={() => <Redirect to={`${rootUrl}/app/table`} />}
          />
          <Route
            path={`${rootUrl}/app/table`}
            render={() =>
              <VirtualizedTable
                list={Immutable.List(this.props.clientFSResults)}
                clientFSState={this.props.clientFSState}
                clientFSSortResults={this.props.clientFSSortResults}
                perspectiveID={perspective.id}
                layoutConfig={layoutConfig}
                screenSize={this.props.screenSize}
              />}
          />
          <Route
            path={`${rootUrl}/app/map_clusters`}
            render={() =>
              <LeafletMap
                handlePosition = {(position) => this.handlePosition(position)}
                center={this.state.position}
                handleZoom = {(level) => this.handleZoom(level)}
                zoom={this.state.zoomLevel}
                results={this.props.clientFSResults}
                layers={this.props.leafletMap}
                pageType='clientFSResults'
                mapMode='cluster'
                facetUpdateID={this.props.clientFSState.facetUpdateID}
                showMapModeControl={false}
                fetchGeoJSONLayers={this.props.fetchGeoJSONLayersBackend}
                clearGeoJSONLayers={this.props.clearGeoJSONLayers}
                fetchByURI={this.props.fetchByURI}
                fetching={false}
                showInstanceCountInClusters={false}
                updateFacetOption={this.props.updateFacetOption}
                showExternalLayers
                facetedSearchMode='clientFS'
                perspectiveID={perspective.id}
                layoutConfig={layoutConfig}
                screenSize={this.props.screenSize}
              />}
          />
          <Route
            path={`${rootUrl}/app/map_markers`}
            render={() => {
              if (this.props.clientFSResults.length > 500) {
                return <ResultInfo message={intl.get('leafletMap.tooManyResults')} />
              } else {
                return (
                  <LeafletMap
                    handlePosition = {(position) => this.handlePosition(position)}
                    center={this.state.position}
                    handleZoom = {(level) => this.handleZoom(level)}
                    zoom={this.state.zoomLevel}
                    results={this.props.clientFSResults}
                    layers={this.props.leafletMap}
                    pageType='clientFSResults'
                    mapMode='marker'
                    facetUpdateID={this.props.clientFSState.facetUpdateID}
                    showMapModeControl={false}
                    fetchGeoJSONLayers={this.props.fetchGeoJSONLayersBackend}
                    clearGeoJSONLayers={this.props.clearGeoJSONLayers}
                    fetchByURI={this.props.fetchByURI}
                    fetching={false}
                    showInstanceCountInClusters={false}
                    updateFacetOption={this.props.updateFacetOption}
                    showExternalLayers
                    facetedSearchMode='clientFS'
                    perspectiveID={perspective.id}
                    layoutConfig={layoutConfig}
                    screenSize={this.props.screenSize}
                  />
                )
              }
            }}
          />
          <Route
            path={`${rootUrl}/app/heatmap`}
            render={() =>
              <GMap results={this.props.clientFSResults} />}
          />
          <Route
            path={`${rootUrl}/app/statistics`}
            render={() =>
              <Pie
                data={this.props.clientFSResults}
                groupBy={this.props.clientFSState.groupBy}
                groupByLabel={this.props.clientFSState.groupByLabel}
                query={this.props.clientFSState.query}
                layoutConfig={layoutConfig}
              />}
          />
          <Route
            path={`${rootUrl}/app/download`}
            render={() =>
              <CSVButton results={this.props.clientFSResults} layoutConfig={layoutConfig} /> }
          />
        </>
      )
    }

  }
}

Places.propTypes = {
  routeProps: PropTypes.object.isRequired,
  perspective: PropTypes.object.isRequired,
  screenSize: PropTypes.string.isRequired,
  clientFSState: PropTypes.object.isRequired,
  clientFSResults: PropTypes.array,
  clientFSSortResults: PropTypes.func.isRequired,
  leafletMap: PropTypes.object.isRequired,
  fetchGeoJSONLayersBackend: PropTypes.func.isRequired,
  clearGeoJSONLayers: PropTypes.func.isRequired,
  rootUrl: PropTypes.string.isRequired
}

export default Places
