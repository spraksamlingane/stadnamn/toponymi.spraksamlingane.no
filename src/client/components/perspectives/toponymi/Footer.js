import React from 'react'
import Paper from '@material-ui/core/Paper'
import PropTypes from 'prop-types'
import Grid from '@material-ui/core/Grid'
import { withStyles } from '@material-ui/core/styles'
import heldigLogo from '../../../img/logos/heldig-logo-small.png'
import secoLogo from '../../../img/logos/seco-logo-white-no-background-small.png'
import uibSpraakLogo from '../../../img/logos/spraak-uib.png' ;
import KVLogo from '../../../img/logos/kartverket.png';

const styles = theme => ({
  root: {
    backgroundColor: theme.palette.primary.main,
    position: 'absolute',
    [theme.breakpoints.down('sm')]: {
      display: 'none'
    },
    bottom: 0,
    left: 0,
    boxShadow: '0 -20px 20px -20px #333',
    width: '100%',
    borderRadius: 0
  },
  layout: {
    width: 'auto',
    // height: 115,
    marginTop: theme.spacing(1),
    marginBottom: theme.spacing(1),
    marginLeft: theme.spacing(2),
    marginRight: theme.spacing(2),
    [theme.breakpoints.up(1500 + theme.spacing(6))]: {
      width: 1500,
      marginLeft: 'auto',
      marginRight: 'auto'
    }
  },
  logoContainer: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center'
  },
  logo: {
    height: 40
  },
  secoLogo: {
    height: 34
  },
  uibSpraakLogo: {
    height: 45
  },
  heldigLogo: {
    height: 33
  },
  KVLogo: {
    height: 45
  }
})

const Footer = props => {
  const { classes } = props
  return (
    <Paper className={classes.root}>
      <Grid container className={classes.layout}>
        <Grid container spacing={3} item xs={12}>
          <Grid item xs className={classes.logoContainer}>
            <a href='https://www.uib.no/ub/101277/norsk-kulturarv-i-skrift-og-tale' target='_blank' rel='noopener noreferrer'>
              <img className={classes.uibSpraakLogo} src={uibSpraakLogo} alt='logo for University of Bergen' />
            </a>
          </Grid>
          <Grid item xs className={classes.logoContainer}>
            <a href='https://kartverket.no/' target='_blank' rel='noopener noreferrer'>
              <img className={classes.KVLogo} src={KVLogo} alt='Kartverkets logo' />
            </a>
          </Grid>
          <Grid item xs className={classes.logoContainer}>
            <a href='https://www.helsinki.fi/en/helsinki-centre-for-digital-humanities' target='_blank' rel='noopener noreferrer'>
              <img className={classes.heldigLogo} src={heldigLogo} alt='Heldig logo' />
            </a>
          </Grid>
          <Grid item xs className={classes.logoContainer}>
            <a href='https://seco.cs.aalto.fi/' target='_blank' rel='noopener noreferrer'>
              <img className={classes.Secologo} src={secoLogo} alt='logo' />
            </a>
          </Grid>
        </Grid>
      </Grid>
    </Paper>
  )
}

Footer.propTypes = {
  classes: PropTypes.object.isRequired
}

export default withStyles(styles)(Footer)
