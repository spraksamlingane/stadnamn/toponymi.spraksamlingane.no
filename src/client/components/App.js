import React from 'react';
import { MuiThemeProvider, createTheme } from '@material-ui/core/styles';
import SemanticPortal from '../containers/SemanticPortal';
// import green from '@material-ui/core/colors/green';
// import deepOrange from '@material-ui/core/colors/deepOrange';
import red from '@material-ui/core/colors/red';
// import amber from '@material-ui/core/colors/amber';
// import grey from '@material-ui/core/colors/grey';

const theme = createTheme({
  palette: {
    primary: red,
  },
  overrides: {
    MuiTooltip: {
      tooltip: {
        fontSize: '1 rem'
      }
    },
    MuiAccordion: {
      root: {
        '&$expanded': {
          marginTop: 8,
          marginBottom: 8
        }
      }
    },
    MuiButton: {
      endIcon: {
        marginLeft: 0
      }
    }
  }
})

const App = () => (
  <MuiThemeProvider theme={theme}>
    <SemanticPortal />
  </MuiThemeProvider>
)

export default App
