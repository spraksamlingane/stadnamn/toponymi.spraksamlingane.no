import React from 'react'
import Immutable from 'immutable'
import PropTypes from 'prop-types'
import intl from 'react-intl-universal'
import { withStyles } from '@material-ui/core/styles'
import IconButton from '@material-ui/core/IconButton'
import PlaceIcon from '@material-ui/icons/Place'
import LaunchIcon from '@material-ui/icons/Launch'
import LODIcon  from '../../img/logos/rdf-resource-description-framework-symbol_icon-icons.com_71374.svg'
import { has } from 'lodash'
import MiradorViewer from './MiradorViewer'
import {
  AutoSizer,
  Column,
  Table,
  SortIndicator
} from 'react-virtualized'

// https://github.com/bvaughn/react-virtualized/issues/650
// https://github.com/bvaughn/react-virtualized/blob/master/docs/usingAutoSizer.md

const styles = theme => ({
  root: props => ({
    height: 500,
    [theme.breakpoints.up(props.layoutConfig.hundredPercentHeightBreakPoint)]: {
      height: `calc(100% - ${props.layoutConfig.tabHeight}px)`
    },
    borderTop: '1px solid rgb(224, 224, 224)',
    backgroundColor: theme.palette.background.paper,
    '& a': {
      textDecoration: 'underline'
    }
  }),
  resultsInfo: {
    flexGrow: 0
  }
})

const tableStyles = {
  tableRoot: {
    fontFamily: 'Noto Sans'
  },
  headerRow: {
    textTransform: 'none'
    // borderBottom: '1px solid rgba(224, 224, 224, 1)'
  },
  labelSeparator: {
    borderRight: '1px solid rgba(224, 224, 224, 1)'
  },
  evenRow: {
    borderBottom: '1px solid rgba(224, 224, 224, 1)'
    // backgroundColor: '#fafafa'
  },
  oddRow: {
    borderBottom: '1px solid rgba(224, 224, 224, 1)'
  },
  noRows: {
    position: 'absolute',
    top: 0,
    bottom: 0,
    left: 0,
    right: 0,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    fontSize: '1em',
    color: '#bdbdbd'
  },
  labelCell: {
    width: '100%', 
    display: 'inline-flex'
  },
  labelCellLeft: {
    float: 'left',
    overflow:'hidden',
    textOverflow: 'ellipsis'
  },
  labelCellRight: {
    float:'right',
    marginRight: '6px',
    marginLeft: 'auto'
  },
  labelCellWrap: {
    display: 'table',
  }
}

const columnWidth = 180

const calculateRowStyle = ({ index }) => {
  if (index < 0) {
    return tableStyles.headerRow
  } else {
    return index % 2 === 0 ? tableStyles.evenRow : tableStyles.oddRow
  }
}

/**
 * A component for displaying large facet result sets as a virtualized table, without pagination.
 * Based on React Virtualized.
 */
class VirtualizedTable extends React.PureComponent {
  constructor (props) {
    super(props)
    this._noRowsRenderer = this._noRowsRenderer.bind(this)
    this._sort = this._sort.bind(this)
  }

  render () {
    const { classes, list, perspectiveID } = this.props
    const rowGetter = ({ index }) => this._getDatum(list, index)

    const headerRenderer = ({
      dataKey,
      label,
      sortBy,
      sortDirection
    }) => {
      const showSortIndicator = sortBy === dataKey
      const children = [
        <span
          className='ReactVirtualized__Table__headerTruncatedText'
          style={showSortIndicator ? {} : { marginRight: 16 }}
          key='label'
          title={label}
        >
          {label}
        </span>
      ]
      if (showSortIndicator) {
        children.push(
          <SortIndicator key='SortIndicator' sortDirection={sortDirection} />
        )
      }
      return children
    }

    const labelRenderer = ({ cellData, rowData }) => {
      if (cellData == null) return ''
      let marker = ''
      let link = ''
      let iiif = ''

      let label = ( <span>{cellData}</span> )
      if (typeof rowData.source !== 'undefined' && rowData.source !== '-') {
        label = ( <a title = {intl.get(`perspectives.${perspectiveID}.properties.source.label`)}
                     target='_blank' rel='noopener noreferrer' href={rowData.source}>{cellData}</a> )
      }

      if (typeof rowData.lat !== 'undefined' || typeof rowData.long !== 'undefined') {
        marker = (
          <IconButton title = {intl.get(`perspectives.${perspectiveID}.properties.coordinateInfo.label`)} 
                      
                      size='small'>
            <PlaceIcon style={{color: "black"}} fontSize="inherit"/>
          </IconButton>
        )
      }

      if (rowData.manifest !== '' && rowData.manifest !== undefined) {
        iiif = (
          <span key={rowData.id}>
          <MiradorViewer manifest={rowData.manifest} title = {intl.get(`perspectives.${perspectiveID}.properties.manifest.label`)}/>
        </span>
        )
      }

      if (typeof rowData.seeAlso !== 'undefined' && rowData.seeAlso !== '-') {
        
        link = (
          <IconButton title = {intl.get(`perspectives.${perspectiveID}.properties.seeAlso.label`)} 
                      onClick = {(e) => window.open(rowData.seeAlso, "_blank")}
                      size='small' >
            <LaunchIcon style={{color: "black"}} fontSize="inherit"/>
            </IconButton>

        )
      }

      const lod = (
          <IconButton title = {intl.get(`perspectives.${perspectiveID}.properties.landingPage.label`)} 
                      onClick = {(e) => window.open(rowData.id, "_blank")}
                      size='small'>
          <img src ={LODIcon} height="14px"/>
            </IconButton>
      )

      return (
        <div key={rowData.id} style={(this.props.screenSize == 'xs') ? tableStyles.labelCellWrap: tableStyles.labelCell}>
           
        <span style={(this.props.screenSize == 'xs') ? tableStyles.labelCellWrap: tableStyles.labelCellLeft}>
          {label}
        </span>
        <span style={(this.props.screenSize == 'xs') ? tableStyles.labelCellWrap: tableStyles.labelCellRight}>{marker}{link}{iiif}{lod}</span>
       
        </div>
      )
    }


    return (
      <div className={classes.root}>
        {this.props.list.size > 0 &&
          <AutoSizer>
            {({ height, width }) => (
              <Table
                overscanRowCount={10}
                rowHeight={50}
                rowGetter={rowGetter}
                rowCount={this.props.list.size}
                sort={this._sort}
                sortBy={this.props.clientFSState.sortBy}
                sortDirection={this.props.clientFSState.sortDirection.toUpperCase()}
                width={width}
                height={height}
                headerHeight={50}
                noRowsRenderer={this._noRowsRenderer}
                style={tableStyles.tableRoot}
                rowStyle={calculateRowStyle}
              >
                <Column
                  style = { tableStyles.labelSeparator }
                  headerStyle = { tableStyles.labelSeparator }
                  label={intl.get(`perspectives.${perspectiveID}.properties.prefLabel.label`)}
                  cellDataGetter={({ rowData }) => rowData.prefLabel}
                  dataKey='prefLabel'
                  headerRenderer={headerRenderer}
                  cellRenderer={labelRenderer}
                  width={columnWidth + 200}
                />
                <Column
                  label={intl.get(`perspectives.${perspectiveID}.properties.typeLabel.label`)}
                  cellDataGetter={({ rowData }) => has(rowData, 'typeLabel') ? rowData.typeLabel.toLowerCase() : ''}
                  dataKey='typeLabel'
                  headerRenderer={headerRenderer}
                  width={columnWidth + 10}
                />
                <Column
                  label={intl.get(`perspectives.${perspectiveID}.properties.admID.label`)}
                  cellDataGetter={({ rowData }) => rowData.admID}
                  dataKey='admID'
                  headerRenderer={headerRenderer}
                  width={columnWidth}
                />
                <Column
                  label={intl.get(`perspectives.${perspectiveID}.properties.fylkeLabel.label`)}
                  cellDataGetter={({ rowData }) => rowData.fylkeLabel}
                  dataKey='fylkeLabel'
                  headerRenderer={headerRenderer}
                  width={columnWidth}
                />
                <Column
                  label={intl.get(`perspectives.${perspectiveID}.properties.kommuneLabel.label`)}
                  cellDataGetter={({ rowData }) => rowData.kommuneLabel}
                  dataKey='kommuneLabel'
                  headerRenderer={headerRenderer}
                  width={columnWidth + 10}
                />
                <Column
                  label={intl.get(`perspectives.${perspectiveID}.properties.collectionYear.label`)}
                  cellDataGetter={({ rowData }) => rowData.collectionYear}
                  dataKey='collectionYear'
                  headerRenderer={headerRenderer}
                  width={columnWidth}
                />
                <Column
                  label={intl.get(`perspectives.${perspectiveID}.properties.datasetCode.label`)}
                  cellDataGetter={({ rowData }) => rowData.datasetCode}
                  dataKey='datasetCode'
                  headerRenderer={headerRenderer}
                  width={columnWidth}
                />


              </Table>
            )}
          </AutoSizer>}
      </div>
    )
  }

  _getDatum (list, index) {
    return list.get(index % list.size)
  }

  _getRowHeight ({ index }) {
    const list = this.props.list
    return this._getDatum(list, index).size
  }

  _noRowsRenderer () {
    return <div className={tableStyles.noRows}>No rows</div>
  }

  // _onScrollToRowChange(event) {
  //   const {rowCount} = this.state;
  //   let scrollToIndex = Math.min(
  //     rowCount - 1,
  //     parseInt(event.target.value, 10),
  //   );
  //
  //   if (isNaN(scrollToIndex)) {
  //     scrollToIndex = undefined;
  //   }
  //
  //   this.setState({scrollToIndex});
  // }

  // https://stackoverflow.com/questions/40412114/how-to-do-proper-column-filtering-with-react-virtualized-advice-needed
  _sort ({ sortBy, event, sortDirection }) {
    if (has(event.target, 'className') && event.target.className.startsWith('Mui')) {
      event.stopPropagation()
    } else {
      this.props.clientFSSortResults({ sortBy, sortDirection: sortDirection.toLowerCase() })
    }
  }
}

VirtualizedTable.propTypes = {
  classes: PropTypes.object.isRequired,
  list: PropTypes.instanceOf(Immutable.List).isRequired,
  clientFSState: PropTypes.object,
  clientFSSortResults: PropTypes.func,
  perspectiveID: PropTypes.string.isRequired
}

export const VirtualizedTableComponent = VirtualizedTable

export default withStyles(styles)(VirtualizedTable)
