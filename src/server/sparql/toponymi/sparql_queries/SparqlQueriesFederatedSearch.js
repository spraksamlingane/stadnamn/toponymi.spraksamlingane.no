let sparqlServer = "http://158.39.48.37" // Test server as default
if (typeof process.env.TOPONYMI_SPARQL !== "undefined") {
  sparqlServer = process.env.TOPONYMI_SPARQL
}


export const federatedSearchDatasets = {
  bustadnamnregisteret: {
    endpoint: 'http://158.39.48.37/stedsnavn-data/query',
    resultQuery: `
    PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
    PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
    PREFIX text: <http://jena.apache.org/text#>
    PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>
    PREFIX ecrm: <http://erlangen-crm.org/current/>
    PREFIX skosxl: <http://www.w3.org/2008/05/skos-xl#>
    PREFIX dct: <http://purl.org/dc/terms/>
    PREFIX wgs84: <http://www.w3.org/2003/01/geo/wgs84_pos#>
    PREFIX spatial: <http://jena.apache.org/spatial#>
    PREFIX skos: <http://www.w3.org/2008/05/skos#>
        
    SELECT distinct ?id ?typeLabel ?prefLabel ?kommune_uri ?fylke_uri ?kommuneLabel ?fylkeLabel ?source ?markerColor ?type_uri ?lat ?long ?manifest ?score ?positioningAccuracy ?datasetCode
    WHERE 
      {    GRAPH <http://data.stadnamn.uib.no/stedsnavn/bustadnamnregisteret>
        {
        {
          SELECT ?identifier ?type_uri ?vokab_fylke ?vokab_kommune ?typeLabel
          WHERE {
            SERVICE <http://158.39.48.37/stedsnavn-vocab> {
              GRAPH <http://data.stadnamn.uib.no/skos/navneliste> {
                ?type_uri dct:identifier ?identifier.
                VALUES (?identifier) {("bruk") ("gard")}.
                ?vokab_fylke dct:identifier "fylke".
                ?vokab_kommune dct:identifier "kommune".
                ?type_uri skosxl:prefLabel/skosxl:literalForm ?typeLabel .
                }
              }
            }
          }
          GRAPH <http://data.stadnamn.uib.no/stedsnavn/bustadnamnregisteret>
          {
            {
              SELECT ?id ?prefLabel ?score WHERE {
                <QUERY>
                ?id rdfs:label ?prefLabel ;
                a ecrm:E53_Place .
                FILTER (NOT EXISTS {?id ecrm:P10_contains ?contains.})
              }
            }
            OPTIONAL {
              ?id ecrm:P131_is_identified_by ?Note.
              ?Note ecrm:P2_has_type <http://vocab.getty.edu/page/aat/300027201> .
              ?Note ecrm:P129i_is_subject_of ?manifest.
              ?manifest dct:conformsTo "http://iiif.io/api/presentation".
            }            
           ?id ecrm:P10_falls_within* ?fylke_uri;
           ecrm:P10_falls_within* ?kommune_uri.
           ?id ecrm:P2_has_type ?type_uri.
           BIND ("BNR" AS ?source)  
           BIND ("yellow" AS  ?markerColor)
           BIND ("BSN" as ?datasetCode)
          OPTIONAL {
          ?id wgs84:lat ?lat.
          ?id wgs84:long ?long.
          }       
        ?fylke_uri ecrm:P2_has_type ?vokab_fylke;      
        rdfs:label ?fylkeLabel.
        ?kommune_uri ecrm:P2_has_type ?vokab_kommune;
        rdfs:label ?kommuneLabel.
        OPTIONAL {
          { SELECT ?geoCoordinatesConcept2 ?positioningAccuracy ?id
          WHERE {
            SERVICE <http://158.39.48.37/stedsnavn-vocab> {
            GRAPH <http://data.stadnamn.uib.no/skos/koordinattype> { 
             VALUES (?geoCoordinatesConcept2) {
               (<http://vokab.toponymi.spraksamlingane.no/id/240bcd1a-9430-3969-8c91-4918399db6b6>)
               (<http://vokab.toponymi.spraksamlingane.no/id/4c74dfb7-fbd9-3733-a3ae-26f8cacf308f>) 
               (<http://vokab.toponymi.spraksamlingane.no/id/430778a1-c65a-394b-b358-f8d73723eaa9>)
               (<http://vokab.toponymi.spraksamlingane.no/id/2039d02a-1008-38bf-9657-3bacbbf3285d>)
               (<http://vokab.toponymi.spraksamlingane.no/id/00e84d2e-7abb-3c88-81a9-9b9f10edd841>)
              }
            ?geoCoordinatesConcept2 skos:prefLabel ?positioningAccuracy.
            }          
          }
            { SELECT ?id ?geoCoordinatesConcept2 WHERE {
              GRAPH <http://data.stadnamn.uib.no/stedsnavn/bustadnamnregisteret> { 
              <QUERY>  
              ?id ecrm:P131_is_identified_by/ecrm:P2_has_type ?geoCoordinatesConcept2.  
            }
          }
        }
      }
    }
  } 
}        
}
}
        `
  },

  bsn: {
    endpoint: `${sparqlServer}/stedsnavn-data/query`,
    resultQuery: `
PREFIX  wgs84: <http://www.w3.org/2003/01/geo/wgs84_pos#>
PREFIX  dct:  <http://purl.org/dc/terms/>
PREFIX  rdf:  <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX  skosxl: <http://www.w3.org/2008/05/skos-xl#>
PREFIX  ecrm: <http://erlangen-crm.org/current/>
PREFIX  xsd:  <http://www.w3.org/2001/XMLSchema#>
PREFIX  skos: <http://www.w3.org/2008/05/skos#>
PREFIX  rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX  text: <http://jena.apache.org/text#>
PREFIX  spatial: <http://jena.apache.org/spatial#>

SELECT DISTINCT  ?id ?typeLabel ?prefLabel ?kommune_uri ?fylke_uri ?datasetCode ?fylkeLabel ?kommuneLabel ?source ?markerColor ?type_uri ?lat ?long ?manifest ?score ?positioningAccuracy ?admID ?collectionYear
WHERE
  { GRAPH <http://data.stadnamn.uib.no/stedsnavn/bsn>
      { { SELECT  ?identifier ?type_uri ?vokab_fylke ?vokab_kommune ?admID ?typeLabel
          WHERE
            { SERVICE <${sparqlServer}/stedsnavn-vocab>
                { GRAPH <http://data.stadnamn.uib.no/skos/navneliste>
                    { ?type_uri  dct:identifier  ?identifier
                      VALUES ?identifier { "bruk" "gard" "eiendom" }
                      ?vokab_fylke  dct:identifier  "fylke" .
                      ?vokab_kommune
                                dct:identifier  "kommune" .
                      ?type_uri skosxl:prefLabel/skosxl:literalForm ?typeLabel .
                    }
                }
            }
        }
        GRAPH <http://data.stadnamn.uib.no/stedsnavn/bsn>
          { { SELECT  ?id ?prefLabel ?score
              WHERE
                { <QUERY>
                  ?id  rdf:type    ecrm:E53_Place;
                  rdfs:label  ?prefLabel .
                }
            }
            OPTIONAL
              { ?id       ecrm:P70I_is_documented_in  ?Note .
                ?Note     ecrm:P2_has_type      <http://vocab.getty.edu/page/aat/300027201> ;
                          ecrm:P129i_is_subject_of  ?manifest .
              }
            
            ?id (ecrm:P89_falls_within)* ?kommune_uri .
            ?kommune_uri  ecrm:P2_has_type  ?vokab_kommune ;
            rdfs:label        ?kommuneLabel .
            ?kommune_uri ecrm:P89_falls_within ?fylke_uri .
            ?fylke_uri  ecrm:P2_has_type  ?vokab_fylke ;
            rdfs:label        ?fylkeLabel .
            ?id  ecrm:P2_has_type  ?type_uri .
            ?dataset skos:notation ?datasetCode .
            OPTIONAL { ?id dct:date ?collectionYear }
            BIND("yellow" AS ?markerColor)
            OPTIONAL { ?id dct:identifier ?admID } 
            OPTIONAL
              { { SELECT  ?geoCoordinatesConcept2 ?positioningAccuracy ?id ?lat ?long
                  WHERE
                    { SERVICE <${sparqlServer}/stedsnavn-vocab>
                        { GRAPH <http://data.stadnamn.uib.no/skos/koordinattype>
                            { 
                              ?geoCoordinatesConcept2
                                        skos:prefLabel  ?positioningAccuracy
                            }
                        }
                      { SELECT  ?id ?geoCoordinatesConcept2 ?lat ?long
                        WHERE
                          { GRAPH <http://data.stadnamn.uib.no/stedsnavn/bsn>
                              { <QUERY>
                                ?id  ecrm:P87_is_identified_by/wgs84:lat   ?lat ;
                                     ecrm:P87_is_identified_by/wgs84:long  ?long .
                                ?id ecrm:P87_is_identified_by/ecrm:P2_has_type ?geoCoordinatesConcept2 }
                          }
                      }
                    }
                }
              }
          }
      }
  }
        `
  },
  sognogfjordane: {
    endpoint: `http://158.39.48.37/stedsnavn-sof-data/query`,
    resultQuery: `
    PREFIX farkplacename: <https://ontology.fylkesarkivet.no/placename#>
    PREFIX text: <http://jena.apache.org/text#>
    PREFIX spatial: <http://jena.apache.org/spatial#>

    PREFIX farkplacename: <https://ontology.fylkesarkivet.no/placename#>
    PREFIX text: <http://jena.apache.org/text#>
    PREFIX spatial: <http://jena.apache.org/spatial#>
    SELECT distinct ?id  ?typeLabel ?prefLabel ?fylkeLabel ?kommuneLabel ?datasetCode ?seeAlso ?markerColor ?lat ?long
      WHERE {   GRAPH <http://data.stadnamn.uib.no/stedsnavn/sogn-og-fjordane> {
            {       SELECT distinct ?id WHERE {
         <QUERY>
         } }
         ?id farkplacename:Naturkode ?typeLabel;
         farkplacename:Normform ?prefLabel ;
         farkplacename:Fylke ?fylkeLabel ;
         farkplacename:Kommune ?kommuneLabel ;
         farkplacename:Wgs84Epsg4326Latitude ?lat ;
         farkplacename:Wgs84Epsg4326Longitude ?long .

         BIND ("SOF" AS ?datasetCode).
         BIND ("blue" AS ?markerColor ).
     }}
        `
  },
  tgn: {
    // Getty LOD documentation:
    // http://vocab.getty.edu/queries#Places_by_Type
    // https://groups.google.com/forum/#!topic/gettyvocablod/r4wsSJyne84
    // https://confluence.ontotext.com/display/OWLIMv54/OWLIM-SE+Full-text+Search
    // http://vocab.getty.edu/queries#Combination_Full-Text_and_Exact_String_Match
    // http://vocab.getty.edu/doc/#TGN_Place_Types
    endpoint: 'http://vocab.getty.edu/sparql.json',
    resultQuery: `
        SELECT ?id (COALESCE(?labelEn,?labelGVP) AS ?prefLabel) ?typeLabel
          ?fylkeLabel ?datasetCode ?seeAlso ?lat ?long ?markerColor
        WHERE {
            ?id luc:term "<QUERYTERM>" ;
            skos:inScheme tgn: ;
            gvp:placeTypePreferred [
              gvp:prefLabelGVP [
                xl:literalForm ?typeLabel;
                dct:language gvp_lang:en
              ]
            ];
            gvp:broaderPreferred/xl:prefLabel/xl:literalForm ?fylkeLabel .
          OPTIONAL {
            ?id xl:prefLabel [
              xl:literalForm ?labelEn ;
              dct:language gvp_lang:en
            ]
          }
          OPTIONAL {
            ?id gvp:prefLabelGVP [xl:literalForm ?labelGVP]
          }
          OPTIONAL {
            ?id foaf:focus ?place .
            ?place wgs:lat ?lat ;
                   wgs:long ?long .
          }
          FILTER EXISTS {
            ?id xl:prefLabel/gvp:term+?term .
            FILTER (LCASE(STR(?term))="<QUERYTERM>")
          }
          BIND("TGN" AS ?datasetCode)
          BIND("orange" AS ?markerColor)
        }
        `
  },
  m1886: {endpoint : `${sparqlServer}/stedsnavn-data/query`,
  resultQuery: `
  PREFIX  wgs84: <http://www.w3.org/2003/01/geo/wgs84_pos#>
  PREFIX  dct:  <http://purl.org/dc/terms/>
  PREFIX  rdf:  <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
  PREFIX  skosxl: <http://www.w3.org/2008/05/skos-xl#>
  PREFIX  ecrm: <http://erlangen-crm.org/current/>
  PREFIX  xsd:  <http://www.w3.org/2001/XMLSchema#>
  PREFIX  skos: <http://www.w3.org/2008/05/skos#>
  PREFIX  rdfs: <http://www.w3.org/2000/01/rdf-schema#>
  PREFIX  text: <http://jena.apache.org/text#>
  PREFIX  spatial: <http://jena.apache.org/spatial#>
  
  SELECT distinct ?id ?typeLabel
  ?prefLabel ?kommune_uri ?fylke_uri ?datasetCode ?fylkeLabel ?kommuneLabel ?source ?seeAlso ?markerColor ?type_uri ?lat ?long ?positioningAccuracy ?collectionYear ?admID
  WHERE
    { GRAPH <http://data.stadnamn.uib.no/stedsnavn/m1886>
        { { SELECT  ?identifier ?type_uri ?vokab_fylke ?vokab_kommune ?typeLabel
            WHERE
              { SERVICE <${sparqlServer}/stedsnavn-vocab>
                  { GRAPH <http://data.stadnamn.uib.no/skos/navneliste>
                      { ?type_uri  dct:identifier  ?identifier
                        VALUES ?identifier { "bruk" "gard" }
                        ?vokab_fylke  dct:identifier  "fylke" .
                        ?vokab_kommune dct:identifier  "kommune" .
                        ?type_uri skosxl:prefLabel/skosxl:literalForm ?typeLabel .
                      }
                  }
              }
          }
          GRAPH <http://data.stadnamn.uib.no/stedsnavn/m1886>
            { { SELECT  ?id ?prefLabel ?score
                WHERE
                  { <QUERY>
                    ?id  a ecrm:E53_Place;
                         rdfs:label  ?prefLabel .
                  }
              }
              ?id ecrm:P2_has_type  ?type_uri .
              ?id ecrm:P70I_is_documented_in ?source .
              BIND("grey" AS ?markerColor)
              OPTIONAL {?id dct:identifier ?admID }
              ?id dct:date ?collectionYear .
              OPTIONAL { ?id ecrm:P67I_is_referred_to_by ?seeAlso . }
              ?id (ecrm:P89_falls_within)* ?kommune_uri .
              ?kommune_uri  ecrm:P2_has_type  ?vokab_kommune ;
                        rdfs:label        ?kommuneLabel .
              ?kommune_uri ecrm:P89_falls_within ?fylke_uri .
              
              ?fylke_uri  ecrm:P2_has_type  ?vokab_fylke ;
                        rdfs:label        ?fylkeLabel .
              ?dataset skos:notation ?datasetCode .

  
              OPTIONAL
                { { SELECT  ?geoCoordinatesConcept2 ?positioningAccuracy ?id ?lat ?long
                    WHERE
                      { SERVICE <${sparqlServer}/stedsnavn-vocab>
                          { GRAPH <http://data.stadnamn.uib.no/skos/koordinattype>
                              { 
                                ?geoCoordinatesConcept2
                                          skos:prefLabel  ?positioningAccuracy
                              }
                          }
                        { SELECT  ?id ?geoCoordinatesConcept2 ?lat ?long
                          WHERE
                            { GRAPH <http://data.stadnamn.uib.no/stedsnavn/m1886>
                                { <QUERY>
                                ?id  ecrm:P87_is_identified_by/wgs84:lat   ?lat ;
                                     ecrm:P87_is_identified_by/wgs84:long  ?long .
                                ?id ecrm:P87_is_identified_by/ecrm:P2_has_type ?geoCoordinatesConcept2 }
                            }
                        }
                      }
                  }
                }
            }
        }
    }

       ` },
       mu1950: {endpoint : `${sparqlServer}/stedsnavn-data/query`,
       resultQuery: `
       PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
       PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
       PREFIX text: <http://jena.apache.org/text#>
       PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>
       PREFIX ecrm: <http://erlangen-crm.org/current/>
       PREFIX skosxl: <http://www.w3.org/2008/05/skos-xl#>
       PREFIX dct: <http://purl.org/dc/terms/>
       PREFIX dcat: <http://www.w3.org/ns/dcat#>
       PREFIX wgs84: <http://www.w3.org/2003/01/geo/wgs84_pos#>
       PREFIX spatial: <http://jena.apache.org/spatial#>
       PREFIX skos: <http://www.w3.org/2008/05/skos#>
       PREFIX skosxl: <http://www.w3.org/2008/05/skos-xl#>
     
     SELECT distinct ?id ?typeLabel ?prefLabel ?kommune_uri ?fylke_uri ?fylkeLabel ?kommuneLabel ?datasetCode ?sourceType ?source ?seeAlso ?markerColor ?lat ?long ?manifest ?score ?positioningAccuracy ?collectionYear ?admID
       WHERE
         {    GRAPH <http://data.stadnamn.uib.no/stedsnavn/mu1950>
           {
           {
             SELECT ?identifier ?type_uri ?vokab_fylke ?vokab_kommune ?typeLabel
             WHERE {
               SERVICE <${sparqlServer}/stedsnavn-vocab> {
                 GRAPH <http://data.stadnamn.uib.no/skos/navneliste> {
                   ?type_uri dct:identifier ?identifier.
                   VALUES (?identifier) {("bruk") ("gard")}.
                   ?vokab_fylke dct:identifier "fylke".
                   ?vokab_kommune dct:identifier "kommune".
                   ?type_uri skosxl:prefLabel/skosxl:literalForm ?typeLabel.
                   }
                 }
               }
             }
             GRAPH <http://data.stadnamn.uib.no/stedsnavn/mu1950>
             {
               {
                 SELECT ?id ?prefLabel ?score WHERE {
               <QUERY>
                   ?id rdfs:label ?prefLabel ;
                   a ecrm:E53_Place .
                 }
               }
              ?id ecrm:P89_falls_within* ?fylke_uri;
              ecrm:P89_falls_within* ?kommune_uri.
              ?id ecrm:P2_has_type ?type_uri.
              BIND ("grey" AS  ?markerColor)
              OPTIONAL {?d dct:identifier ?admID }
              ?dataset rdf:type dcat:Dataset .
              ?dataset skos:notation ?datasetCode .
              ?id dct:date ?collectionYear .
              OPTIONAL { ?id ecrm:P67I_is_referred_to_by ?seeAlso . }
              
              BIND ("Matrikkel" AS  ?sourceType)
     
             OPTIONAL {
             ?id ecrm:P87_is_identified_by/wgs84:lat ?lat.
             ?id ecrm:P87_is_identified_by/wgs84:long ?long.
             ?id ecrm:P87_is_identified_by/ecrm:P2_has_type ?positioningAccuracy.
             }
           ?fylke_uri ecrm:P2_has_type ?vokab_fylke;
           rdfs:label ?fylkeLabel.
           ?kommune_uri ecrm:P2_has_type ?vokab_kommune;
           rdfs:label ?kommuneLabel.
          }
     }
     }
     
            ` },
  rygh: {endpoint : `${sparqlServer}/stedsnavn-data/query`,
  resultQuery: `
  PREFIX  wgs84: <http://www.w3.org/2003/01/geo/wgs84_pos#>
  PREFIX  dct:  <http://purl.org/dc/terms/>
  PREFIX  rdf:  <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
  PREFIX  skosxl: <http://www.w3.org/2008/05/skos-xl#>
  PREFIX  ecrm: <http://erlangen-crm.org/current/>
  PREFIX  xsd:  <http://www.w3.org/2001/XMLSchema#>
  PREFIX  skos: <http://www.w3.org/2008/05/skos#>
  PREFIX  rdfs: <http://www.w3.org/2000/01/rdf-schema#>
  PREFIX  text: <http://jena.apache.org/text#>
  PREFIX  spatial: <http://jena.apache.org/spatial#>
  
  SELECT distinct ?id (GROUP_CONCAT (DISTINCT ?typeLabelX; separator = '/') AS ?typeLabel)
  ?prefLabel ?kommune_uri ?fylke_uri ?datasetCode ?fylkeLabel ?kommuneLabel ?source ?markerColor ?type_uri ?lat ?long ?positioningAccuracy ?collectionYear ?admID
  WHERE
    { GRAPH <http://data.stadnamn.uib.no/stedsnavn/rygh>
        { { SELECT  ?identifier ?type_uri ?vokab_fylke ?vokab_kommune ?typeLabelX
            WHERE
              { SERVICE <${sparqlServer}/stedsnavn-vocab>
                  { GRAPH <http://data.stadnamn.uib.no/skos/navneliste>
                      { ?type_uri  dct:identifier  ?identifier
                        VALUES ?identifier { "bruk" "gard" "navnegard" "gammelBosettingsplass" "kommune" "sokn" }
                        ?vokab_fylke  dct:identifier  "fylke" .
                        ?vokab_kommune dct:identifier  "kommune" .
                        ?type_uri skosxl:prefLabel/skosxl:literalForm ?typeLabelX .
                      }
                  }
              }
          }
          GRAPH <http://data.stadnamn.uib.no/stedsnavn/rygh>
            { { SELECT  ?id ?prefLabel
                WHERE
                  { <QUERY>
                    ?id  a ecrm:E53_Place;
                         rdfs:label  ?prefLabel .
                  }
              }
              ?dataset skos:notation ?datasetCode .
              ?id ecrm:P2_has_type  ?type_uri .
              ?id ecrm:P70I_is_documented_in ?nbsource .
              ?id (ecrm:P89_falls_within)* ?kommune_uri .
              ?kommune_uri  ecrm:P2_has_type  ?vokab_kommune ;
                        rdfs:label        ?kommuneLabel .
              ?kommune_uri ecrm:P89_falls_within ?fylke_uri .
              
              ?fylke_uri  ecrm:P2_has_type  ?vokab_fylke ;
                        rdfs:label        ?fylkeLabel .
              BIND("violet" AS ?markerColor)
              ?id dct:date ?collectionYear .
              OPTIONAL { ?id dct:identifier ?admID }
              BIND(CONCAT(STR(?nbsource), '&searchText=', ?prefLabel) as ?source) .  
              OPTIONAL
                { { SELECT  ?geoCoordinatesConcept2 ?positioningAccuracy ?id ?lat ?long
                    WHERE
                      { SERVICE <${sparqlServer}/stedsnavn-vocab>
                          { GRAPH <http://data.stadnamn.uib.no/skos/koordinattype>
                              { 
                                ?geoCoordinatesConcept2
                                          skos:prefLabel  ?positioningAccuracy
                              }
                          }
                        { SELECT  ?id ?geoCoordinatesConcept2 ?lat ?long
                          WHERE
                            { GRAPH <http://data.stadnamn.uib.no/stedsnavn/rygh>
                                { <QUERY>
                                  ?id  ecrm:P87_is_identified_by/wgs84:lat   ?lat ;
                                       ecrm:P87_is_identified_by/wgs84:long  ?long .
                                  
                                  ?id ecrm:P87_is_identified_by/ecrm:P2_has_type ?geoCoordinatesConcept2 }
                            }
                        }
                      }
                  }
                }
            }
        }
    }
  GROUP BY ?id ?prefLabel ?kommune_uri ?fylke_uri ?datasetCode ?fylkeLabel ?kommuneLabel ?source ?markerColor ?type_uri ?lat ?long ?manifest ?score ?positioningAccuracy ?collectionYear ?admID

       ` }

}
