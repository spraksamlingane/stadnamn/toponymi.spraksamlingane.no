import { runSelectQuery } from './SparqlApi'
import {
  mapNameSampoResults
} from './Mappers'

const getResults = async (federatedSearchDatasets, queryTerm, latMin, longMin, latMax, longMax, datasetId, resultFormat) => {
  const { endpoint, resultQuery } = federatedSearchDatasets[datasetId]
  const luceneRegex = /\s(AND|OR)\s/g;
  const aposRegex = /'/g;
  let processedQueryTerm = '';
  let query = ''
  let sparqlFilterByScore = '';
  let queryList = [];
  const cleanedQueryTerm = queryTerm.replace(aposRegex,"");
  console.log(cleanedQueryTerm)
  if (cleanedQueryTerm.match(luceneRegex)){
    queryList = [cleanedQueryTerm.toLowerCase()] ; }
  else {queryList = cleanedQueryTerm.toLowerCase().split(/\s+/g).filter(x=>x) ; }
  let processedQueryList = []
  queryList.forEach(word => {
    let processedWord = word
    if (word.includes('å')) {
      processedWord = `(${processedWord})|(${word.replace('å','aa')})`
    } else if (word.includes('aa', 'å')) {
      processedWord = `(${processedWord})|(${word.replace('aa','å')})`
    }
    processedQueryList.push(processedWord)
  })
  processedQueryTerm = processedQueryList.join(" AND ")
  console.log(processedQueryTerm);

  if (queryTerm.includes('*')){
    sparqlFilterByScore = '';
  }
  else if (queryTerm.length < 8){
    sparqlFilterByScore = ''; /*'FILTER (?score > 7.0)'*/
}
  else if (queryTerm.length < 13) {
    sparqlFilterByScore = ''; /*'FILTER (?score > 6.5)';*/
  }
  else {
    sparqlFilterByScore  = ''; /*'FILTER (?score > 5.0)';*/
  }

  if (datasetId !== 'tgn') {
    if (queryTerm !== '') {
      query = resultQuery.replace(/<QUERY>/g, `?id text:query ( '${processedQueryTerm}' 100000) .  ${sparqlFilterByScore}`)
    } else if (latMin !== 0) {
      query = resultQuery.replace(/<QUERY>/g, `?id spatial:withinBox (${latMin} ${longMin} ${latMax} ${longMax} 1000000) .`)
    }
  } else {
    query = resultQuery.replace(/<QUERYTERM>/g, queryTerm.toLowerCase())
  }
  return runSelectQuery({
    query,
    endpoint,
    resultMapper: mapNameSampoResults,
    resultFormat
  })
}

export const getFederatedResults = async ({
  federatedSearchDatasets,
  queryTerm,
  latMin,
  longMin,
  latMax,
  longMax,
  datasets,
  resultFormat
}) => {
  const federatedResults = await Promise.all(datasets.map((datasetId) =>
    getResults(federatedSearchDatasets, queryTerm, latMin, longMin, latMax, longMax, datasetId, resultFormat)))

  // merge search results from multiple endpoints into a single array
  let results = []
  federatedResults.forEach(resultSet => {
    results = [...results, ...resultSet.data]
  })

  return results
}
